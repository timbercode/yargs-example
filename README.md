# yargs-example

Example of [Yargs]( http://yargs.js.org/ ) usage comparing to other
ways of providing script parameters.

Provided scripts are examples of 3 ways to provide run parameters.
They are used as sample code in 
[Yargs! Parametryzacjo skryptu, nie boję się ciebie!]( http://timbercode.pl/blog/2017/01/01/yargs/ )
blog post (polish language).

All those scripts are pretending be a script which load demo data
for some web application through API served by backend. Each of them
requires 3 parameters:
* environment (application instance) on which data should be loaded,
* path to file with data which should be loaded,
* whether existing data should be removed or not. 

Script are safe: they do nothing more than reading 
provided parameters and printing them out to console.

## Setup

1. Install [Node.js]( https://nodejs.org/en/ )
1. Run `npm install` to download all required dependencies. 

## Examples

1. Script with parameters defined inside script as constants:
   ```
   node ./load-data_constants.js
   ```
   To change values of parameters you have to modify script.
   
2. Script with parameters as array with fixed positions, accessed with
  `process.argv`:
   ```
   node ./load-data_array.js staging ./data/geekit-conference.json -7
   ```
   You can omit the last parameter, because`0` is default time zone
   offset:
   ```
   node ./load-data_array.js staging ./data/geekit-conference.json
   ```
    
3. Script with parameters parsed by [Yargs]( http://yargs.js.org/ )
   library:
   ```
   node ./load-data_yargs.js -e staging -d ./data/geekit-conference.json -z -7
   ```
   You can change the order of parameters, and  omit the time zone offset,
   because`0` is a default one:
   ```
   node ./load-data_yargs.js -d ./data/geekit-conference.json -e staging
   ```
   Moreover you can use long names of parameters:
   ```
   node ./load-data_yargs.js \
       --environment staging \
       --dataFile ./data/geekit-conference.json \
       --timeZoneOffset -7
   ```
   If you make mistake or provide no parameters, error message will
   appears (below because of `stage` instead of `staging`):
   ```
   node ./load-data_yargs.js -e stage -d ./data/geekit-conference.json
   ```
    ```
    Options:
      -e, --environment     Which environment to use (will result with URL of API)
      [string] [required] [choices: "localhost", "testing", "staging", "production"]
      -d, --dataFile        Path to file with data to use        [string] [required]
      -z, --timeZoneOffset  Time Zone offset used to adjust timestamps of created
                            data                    [number] [required] [default: 0]
      --help                Show help                                      [boolean]
    
    Invalid values:
      Argument: e, Given: "stage", Choices: "localhost", "testing", "staging", "production"
    ```
   
   
   
