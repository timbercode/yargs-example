const yargs = require('yargs');

const DEFAULT_TIME_ZONE_OFFSET = 0;

const argv = yargs
    .strict()
    .option('e', {
        alias: 'environment',
        type: 'string',
        describe: 'Which environment to use (will result with URL of API)',
        choices: [
            'localhost',
            'testing',
            'staging',
            'production'
        ],
        demand: true
    })
    .option('d', {
        alias: 'dataFile',
        type: 'string',
        describe: 'Path to file with data to use',
        demand: true
    })
    .option('z', {
        alias: 'timeZoneOffset',
        type: 'number',
        describe: 'Time Zone offset used to adjust timestamps of created data',
        default: DEFAULT_TIME_ZONE_OFFSET,
        demand: true
    })
    .help()
    .detectLocale(false)
    .argv;

const ENVIRONMENT = argv['environment'];
const DATA_FILE = argv['dataFile'];
const TIME_ZONE_OFFSET = argv['timeZoneOffset'];

console.log("Provided parameters:");
console.log("ENVIRONMENT      = ", ENVIRONMENT);
console.log("DATA_FILE        = ", DATA_FILE);
console.log("TIME_ZONE_OFFSET = ", TIME_ZONE_OFFSET);
