//const ENVIRONMENT = 'localhost';
// const ENVIRONMENT = 'testing';
const ENVIRONMENT = 'staging';
//const ENVIRONMENT = 'production';

//const DATA_FILE = './data/company-the-big.json';
//const DATA_FILE = './data/that-fancy-startup.json';
const DATA_FILE = './data/geekit-conference.json';

//const TIME_ZONE_OFFSET = 0;
// const TIME_ZONE_OFFSET = +2;
const TIME_ZONE_OFFSET = -7;

console.log("Provided parameters:");
console.log("ENVIRONMENT      = ", ENVIRONMENT);
console.log("DATA_FILE        = ", DATA_FILE);
console.log("TIME_ZONE_OFFSET = ", TIME_ZONE_OFFSET);
