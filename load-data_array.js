const DEFAULT_TIME_ZONE_OFFSET = 0;

const ENVIRONMENT = process.argv[2];
const DATA_FILE = process.argv[3];
const TIME_ZONE_OFFSET = process.argv[4] || DEFAULT_TIME_ZONE_OFFSET;

console.log("Provided parameters:");
console.log("ENVIRONMENT      = ", ENVIRONMENT);
console.log("DATA_FILE        = ", DATA_FILE);
console.log("TIME_ZONE_OFFSET = ", TIME_ZONE_OFFSET);
